class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :mona_addr
      t.string :deposit_mona_addr
      t.integer :amount
      t.datetime :last_faucet

      t.timestamps
    end
  end
end
