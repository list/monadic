class CreateSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :sessions do |t|
      t.integer :account_id
      t.string :session_id
      t.boolean :enable
      t.datetime :expire_date

      t.timestamps
    end
  end
end
