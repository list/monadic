class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def login?
    nil != Session.where(session_id: cookies[:session_id]).first
  end
  def current_account
    # 該当する有効なセッションのユーザ返す
    if login?
      a =Session.where(session_id: cookies[:session_id]).first.account
      a.reload_amount!
      a
    else
      nil
    end
  end
end
