class DiceController < ApplicationController
  def roll
    param = params.permit(:amount,:commit)
    ca = current_account

    @amount = param[:amount].to_i
    if @amount <= 0 || @amount > ca.amount
      return head 400
    end
    @result = rand(6)+1
    @user_ammount = {}
    @user_ammount[:before] = ca.amount

    if((param[:commit]==="High" && @result > 3) || (param[:commit]==="Low" && @result <= 3))
        @win = true
        ca.give_amount @amount.to_i

    else
        @win = false
        ca.drop_amount @amount.to_i
    end
    @user_ammount[:after] = ca.amount
    @profit = (@user_ammount[:before] - @user_ammount[:after]).abs
    ca.save!

  end

end
