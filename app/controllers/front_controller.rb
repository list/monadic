class FrontController < ApplicationController
  def index
    @login = login?
    @account = current_account
  end
  def login
    @mona_addr = params.permit(:mona_addr)[:mona_addr]
    @account = Account.where(:mona_addr=>@mona_addr).first
    if(@account==nil)
      @account = Account.generate @mona_addr
      @account.save!
    end

    @session = @account.login
    cookies[:session_id] = @session.session_id
    redirect_to action: 'index'
  end

  def logout
    cookies[:session_id] = nil
    redirect_to action: 'index'
  end
end
