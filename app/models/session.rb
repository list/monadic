class Session < ApplicationRecord
  belongs_to :account
  def Session.generate account
    s = Session.new
    s.account = account
    s.session_id = Session.generateSessionId
    s.enable = true
    s.save!
    s
  end
  def Session.generateSessionId
    res = ''
    40.times {
      res += (97+rand(25)).chr
    }
    res
  end

end
