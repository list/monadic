require "open3"
class Account < ApplicationRecord
  has_many :sessions
  def Account.generate mona_addr
    a = Account.new
    a.mona_addr = mona_addr
    a.amount = 1000
    a.last_faucet = nil
    a.save!
    a.generate_monacoin_id
    a.give_amount 1000
    a.save!
    a
  end
  def login
    Session.generate self
  end
  def generate_monacoin_id
    if self.deposit_mona_addr==nil
      o, e, s = Open3.capture3("docker exec test_mona monacoin-cli getnewaddress #{self.id}")
      if s==0
        self.deposit_mona_addr = o.strip
      end
    end
  end
  def reload_amount!
      o, e, s = Open3.capture3(" docker exec test_mona monacoin-cli getbalance #{self.id}")
      if s==0
        self.amount = o.strip.to_f * 1_000_000
        self.save!
      end
  end
  def drop_amount volume
    p volume
      unless self.check_amount volume
        return
      end

      v = volume / 1_000_000.0
  #    p "docker exec test_mona monacoin-cli move #{self.id} root #{sprintf("%f",v)}"
      o, e, s = Open3.capture3("docker exec test_mona monacoin-cli move #{self.id} root #{sprintf("%f",v)}")
      self.reload_amount!
  end

  def check_amount volume
    unless(volume.to_s =~ /^[0-9]+$/)
      return false
    end
    self.reload_amount!
    self.amount >= volume
  end

  def give_amount volume
    p volume

      unless Account.check_root_amount volume
        return false
      end

      v = volume / 1_000_000.0
      p "docker exec test_mona monacoin-cli move #{self.id} root #{sprintf("%f",v)}"
      o, e, s = Open3.capture3("docker exec test_mona monacoin-cli move root #{self.id}  #{sprintf("%f",v)}")
      self.reload_amount!
      #if s==0
      #  self.amount = o.strip.to_f*1_000_000
    #  end
  end
  def Account.check_root_amount volume
    p 'c1'
    unless(volume.to_s =~ /^[0-9]+$/)
      return false
    end
    p 'c2'

    o, e, s = Open3.capture3("docker exec test_mona monacoin-cli getbalance root")
    if s==0
      amount = o.to_f * 1_000_000.0
      p amount
      amount >= volume
    else
      return false
    end
  end

end
