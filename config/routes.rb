Rails.application.routes.draw do
  get 'dice/roll'

  get 'front/index'
  post 'front/login'
  post 'front/logout'
  post 'dice/roll'
  root 'front#index'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
